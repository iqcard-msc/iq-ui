define(['angular', 'd3'], function (angular, d3) {
    'use strict';

    function PiechartDirective() {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                items: '=',
                config: '='
            },
            template: '<div></div>',
            link: function (scope, element) {
                var config = angular.extend({}, {
                    outerRadius: 100,
                    innerRadius: 50,
                    duration: 1000,  // ms
                    getData: function (d) {
                        return d;
                    },
                    getColor: function (d, i) {
                        return '#000';
                    }
                }, scope.config);

                var svg = d3.select(element[0]).append('svg')
                    .attr('height', config.outerRadius * 2)
                    .attr('width', config.outerRadius * 2);

                var g = svg.append('g')
                    .attr('transform', 'translate(' + config.outerRadius + ',' + config.outerRadius + ')');

                var pie = d3.layout.pie()
                    .value(config.getData)
                    .sort(null);

                var arc = d3.svg.arc()
                    .outerRadius(config.outerRadius)
                    .innerRadius(config.innerRadius);

                function arcTween(a) {
                    var start = {
                        startAngle: 0,
                        endAngle: 0
                    };
                    var i = d3.interpolate(start, a);
                    return function(t) {
                        return arc(i(t));
                    };
                }

                function render(data) {

                    g.selectAll('path').remove();

                    g.datum(data).selectAll('path')
                        .data(pie)
                        .enter()
                        .append('path')
                        .attr('class', 'arc')
                        .attr('fill', config.getColor)
                        .attr('d', arc);

                    g.datum(data).selectAll('path')
                        .data(pie)
                        .transition()
                        .duration(config.duration)
                        .attrTween('d', arcTween);
                }

                scope.$watch('items', function (newValue) { render(newValue); }, true);
            }
        };
    }

    return PiechartDirective;
});
