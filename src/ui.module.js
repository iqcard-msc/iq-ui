define([
    'angular',
    './daterange-picker.directive.js',
    './piechart.directive.js'
], function (
    angular,
    DaterangePickerDirective,
    PiechartDirective
) {
    'use strict';

    var app = angular.module('iq.ui', []);

    app.directive('iqDaterangePicker', DaterangePickerDirective);
    app.directive('iqPiechart', PiechartDirective);

    return app;
});
