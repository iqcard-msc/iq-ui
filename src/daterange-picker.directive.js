define(['jquery', 'bootstrap-daterangepicker'], function ($) {
    'use strict';

    // Обёртка над календарём https://github.com/dangrossman/bootstrap-daterangepicker
    function DaterangePickerDirective() {
        return {
            restrict: 'A',
            replace: false,
            require: 'ngModel',
            scope: {
                daterangePickerSingle: '@'
            },
            link: function (scope, element, attrs, ngModelCtrl) {
                $(function () {
                    // Если есть атрибут `daterange-picker-single`,
                    // то календарь будет 1 и вместо диапазона будет выбираться только 1 дата
                    var single = scope.daterangePickerSingle ? true : false;
                    $(element).daterangepicker({
                        autoApply: true,
                        showDropdowns: true,
                        drops: 'up',
                        opens: 'center',
                        singleDatePicker: single,
                        locale: {
                            format: 'DD.MM.YYYY'
                        }
                    }).on('apply.daterangepicker', function (ev, picker) {
                        var value;
                        if (single) {
                            // Для одиночного календаря значением является дата
                            value = picker.startDate.toDate();
                        } else {
                            value = {
                                from: picker.startDate.toDate(),
                                to: picker.endDate.toDate(),
                                name: picker.startDate.format('L') + ' - ' + picker.endDate.format('L')
                            };
                        }
                        // Связываем значение календаря с моделью
                        ngModelCtrl.$setViewValue(value);

                        scope.$emit('apply.daterangepicker');
                        scope.$apply();
                    }).on('hide.daterangepicker', function () {
                        scope.$emit('hide.daterangepicker');
                        scope.$apply();
                    });
                });
            }
        };
    }
    return DaterangePickerDirective;
});
