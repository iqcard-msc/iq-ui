require.config({
    paths: {
        'domReady': 'https://cdnjs.cloudflare.com/ajax/libs/require-domReady/2.0.1/domReady',
        'angular': '../bower_components/angular/angular',
        'bootstrap': '../bower_components/bootstrap/dist/js/bootstrap',
        'bootstrap-daterangepicker': '../bower_components/bootstrap-daterangepicker/daterangepicker',
        'd3': '../bower_components/d3/d3',
        'jquery': '../bower_components/jquery/dist/jquery',
        'moment': '../bower_components/moment/moment',
        'iq-ui': '../iq-ui',
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap-daterangepicker': {
            deps: ['bootstrap']
        },
        'd3': {
            exports: 'd3'
        },
        'jquery': {
            exports: '$'
        },
    }
});

require(['domReady!', 'd3', 'angular', 'iq-ui'], function (document, d3, angular) {
    var app = angular.module('demo', ['iq.ui']);

    app.controller('DemoController', ['$scope',
        function ($scope) {
            var vm = this;

            var colors = d3.scale.category10();

            vm.color = function (i) {
                return colors(i);
            };

            vm.items = [
                { id: 1, val: 1 },
                { id: 2, val: 1 },
                { id: 3, val: 2 }
            ];
            var counter =vm.items.length;
            vm.config = {
                duration: 500,
                outerRadius: 100,
                innerRadius: 50,
                getData: function (d) {
                    return d.val;
                },
                getColor: function (d, i) {
                    return vm.color(d.data.id);
                }
            };
            vm.addItem = function () {
                vm.items.push({ id: ++counter, val: 1 });
            };
            vm.removeItem = function (index) {
                vm.items.splice(index, 1);
            };

            // vm.toggleCalendar = function () {
            //     // Когда показывается календарь, временно отключаем закрытие окна по клику вне его
            //     vm.autoClose = 'disabled';
            // };
            //
            // // Когда выбран диапазон из календаря, закрываем и текущий виджет
            // $scope.$on('apply.daterangepicker', function () {
            //     vm.isOpen = false;
            //     // когда диапазон дат выбран, восстанавливаем свойство всплывающего окна закрываться при клике вне его
            //     vm.autoClose = 'outsideClick';
            // });
            // $scope.$on('hide.daterangepicker', function () {
            //     vm.autoClose = 'outsideClick';
            // });
        }
    ]);

    angular.bootstrap(document, ['demo']);
});
