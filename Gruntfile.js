module.exports = function(grunt) {
    'use strict';

    grunt.initConfig({
        ngtemplates: {
            app: {
                cwd: 'src',
                src: '*.html',
                dest: 'src/ui.templates.js',
                options: {
                    prefix: 'ui/',
                    module: 'iq.ui',
                    bootstrap:  function(module, script) {
                        return "" +
                        "define([], function () {\n" +
                        "    function templateCache($templateCache) {\n" +
                        "        " + script + "\n" +
                        "    }\n" +
                        "    templateCache.$inject = ['$templateCache'];\n" +
                        "    return templateCache;\n" +
                        "});";
                    }
                }
            }
        },
        requirejs: {
            compile: {
                options: {
                    baseUrl: './src/',
                    name: 'iq-ui',
                    out: 'iq-ui.js',
                    optimize: 'none',
                    paths: {
                        'angular': 'empty:',
                        'jquery': 'empty:',
                        'bootstrap': 'empty:',
                        'bootstrap-daterangepicker': 'empty:',
                        'd3': 'empty:'
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-angular-templates');

    grunt.registerTask('default', ['ngtemplates', 'requirejs']);
};
